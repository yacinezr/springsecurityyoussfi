package biz.picosoft.springsecurityyoussfi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Table
@Entity
public class Entry {
    @Id
    String imei;
    String model;
    String sn;
//    String status;
}

package biz.picosoft.springsecurityyoussfi.config;


import biz.picosoft.springsecurityyoussfi.entity.AppUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import io.jsonwebtoken.Jwts;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        super();
        this.authenticationManager = authenticationManager;
    }

    //    nous avons la main sur les requette ici  (attemptAuthentication et successfulAuthentication)
    //    c'est la fonction où ce passe l'authentification ,
//    la requete post vers l'uri /login on lui passant le username et password
//    donc on recupere le username et password puis
    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request,
            HttpServletResponse response) throws AuthenticationException {
        AppUser appUser = null;
//        si les donnée d'authentification sont envoyé en formation wwwurlencoded
//        donc on faire le traitement comme suit pour prendre le username et password
//
//        String username = request.getParameter("username");
//        String password = request.getParameter("password");
//
//        si on envoie les donnée de l'authentification en format JSON
//        donc il faut lire le contenue de la requete et faire le mappage du JSON vers un object AppUser
        try {
            appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);
        } catch (Exception e) {
        throw new RuntimeException(e);
    }
        System.out.println("********************************");
        System.out.println("Username   " + appUser.getUsername());
        System.out.println("Password    " + appUser.getPassword());

        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(appUser.getUsername(), appUser.getPassword()));
    }

    //    c'est la où on génére le token
    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication authResult) throws IOException, ServletException {

        User springUser = (User) authResult.getPrincipal();
        String jwtT = Jwts.builder()

                .setSubject(springUser.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME)) //date d'expriration dans 10 jours
                .signWith(SignatureAlgorithm.HS256, SecurityConstants.SECRET) // alg de signature
                .claim("roles", springUser.getAuthorities()) // liste des roles
                .compact();
        response.addHeader(SecurityConstants.HEADER_STRING,SecurityConstants.TOCKEN_PREFIX+jwtT);
    }
}

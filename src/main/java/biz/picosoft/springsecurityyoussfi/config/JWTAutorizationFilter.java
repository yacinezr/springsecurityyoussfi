package biz.picosoft.springsecurityyoussfi.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

// on execute ce filtre avant tous les autre filtre
// Before all request we execute this filtre
public class JWTAutorizationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws ServletException, IOException {
        // en cas d'une requete avec Option il vas répndre avec ça
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Contol-Request-Headers,authorization");
        response.addHeader("Access-Control-Expose-Headers", "Access-Controle-Allow-Origin, Access-Controle-Allow-Credentials, authorization");
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
        }
        ///////////////////////////////////////////////////////////

        String jwt = request.getHeader(SecurityConstants.HEADER_STRING);
        if (jwt == null || !jwt.startsWith(SecurityConstants.TOCKEN_PREFIX)) // s'il n'y a pas du token ou le token ne commence pas par Bearer alors on passe au filtre suivant
        {
            chain.doFilter(request, response); // doFiltre signifie passe au filtre suivant et spring sec va decider ce qu'il faut faire
            return; // je quite la méthode
        }

        // claims c'est l'ensembe des donnés stocker dans le jwt
        // dans si desous on supprime l'encodage puis on on supprime la mot Bearer
        // en fin on fait le mappage vers objet
        Claims claims = Jwts.parser()
                .setSigningKey(SecurityConstants.SECRET) // enlever le base64 codage
                .parseClaimsJws(jwt.replace(SecurityConstants.TOCKEN_PREFIX, ""))
                .getBody();

        String username = claims.getSubject(); // on prend le username qu'on le stocke dans le subject

        ArrayList<Map<String, String>> roles = (ArrayList<Map<String, String>>) claims.get("roles"); // avoir tous les roles dans une map
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        roles.forEach((r -> {
            authorities.add(new SimpleGrantedAuthority(r.get("authority")));
        }));

        // vérifier est ce que le token est valide ou pas

        // remplir l'objet global de spring security principale, on lui fournir le username et les roles
        // username pour connaitre l'utilisateur et role pour savoir ce droit d'execition
        UsernamePasswordAuthenticationToken authenticatedUser = new UsernamePasswordAuthenticationToken(username, null, authorities);
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser); // on charge cet utilisateur authentifier dans le context de security on dit a spring voila cet utilisateur est celui qui a fait l'authentification
        chain.doFilter(request, response);


    }
}

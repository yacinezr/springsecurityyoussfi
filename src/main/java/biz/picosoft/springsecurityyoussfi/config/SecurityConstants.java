package biz.picosoft.springsecurityyoussfi.config;

public class SecurityConstants {
    public static final String SECRET = "zribi.yacine1@gmail.com";
    public static final long EXPIRATION_TIME = 864_000_000; //10 days
    public static final String TOCKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
package biz.picosoft.springsecurityyoussfi.rest;

import biz.picosoft.springsecurityyoussfi.entity.AppUser;
import biz.picosoft.springsecurityyoussfi.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountRestController {
    @Autowired
    private AccountService accountService;

    @PostMapping("/register")
    public AppUser register(@RequestBody AppUser user) {
        return accountService.saveUser(user);
    }
}
